import sys

from PyQt5 import uic
from PyQt5.QtWidgets import QApplication, QMainWindow
import requests

URL = "http://localhost:5000"


class KeyBoard(QMainWindow):
    def __init__(self, keyboard):
        super().__init__()
        uic.loadUi(keyboard, self)
        self.keyboard = keyboard
        for key in self.keysGroup.buttons():
            key.clicked.connect(self.get_encoding_by_keyboard)
        self.changeLanguage_button.clicked.connect(self.change_language)
        self.searchButton.clicked.connect(self.get_encoding_by_input)

    def get_encoding_by_keyboard(self):
        symbols = self.sender().text()
        encoding = requests.get(f'{URL}/get_encoding', json={'symbols': symbols}).json()
        self.encodingLabel.setText(f'Кодировка выбранных символов в системе Unicode: {encoding}')

    def get_encoding_by_input(self):
        symbols = self.symbol_input.text()
        encoding = requests.get(f'{URL}/get_encoding', json={'symbols': symbols}).json()
        self.encodingLabel.setText(f'Кодировка выбранных символов в системе Unicode: {encoding}')

    def change_language(self):
        if self.keyboard == 'russian_keyboard.ui':
            self.english_keyboard = KeyBoard('english_keyboard.ui')
            self.english_keyboard.show()
            self.close()
        else:
            self.russian_keyboard = KeyBoard('russian_keyboard.ui')
            self.russian_keyboard.show()
            self.close()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    russian_keyboard = KeyBoard('russian_keyboard.ui')
    russian_keyboard.show()
    sys.exit(app.exec_())
