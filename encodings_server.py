from flask import Flask, jsonify, request
import sqlite3

app = Flask(__name__)


def get_encoding_from_database(symbols):
    con = sqlite3.connect('encodings_db.db')
    cur = con.cursor()
    result = ''
    for symbol in symbols:
        if symbol == ' ':
            result += ' '
        else:
            encoding = cur.execute(
                f"""SELECT encoding FROM encodings_of_symbols WHERE symbol = '{symbol}'""").fetchall()[0][0]
            result += encoding
    con.close()
    return result


@app.route("/")
def index():
    return "<h1>Run client </h1>"


@app.route('/get_encoding', methods=['GET'])
def get_encoding():
    symbols = request.json['symbols']
    encoding = get_encoding_from_database(symbols)
    if encoding:
        return jsonify(str(encoding))
    return jsonify('Кодировка этих символов не найдена')


if __name__ == "__main__":
    app.run()
